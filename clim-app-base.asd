(defsystem     "clim-app-base"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Substrate for CLIM applications"
  :serial      t
  :depends-on  (:alexandria :serapeum :clim)
  :components  ((:module "src/"
                 :components ((:file "classes")))))

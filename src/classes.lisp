(in-package :cl)
(defpackage :clim-app-base
  (:use :cl)
  (:shadow #:directory)
  (:export #:file #:directory #:absolute-pathname))
(in-package :clim-app-base)

(defclass file ()
  ((%absolute-pathname :initarg :absolute-pathname
                       :accessor absolute-pathname)))

(defclass directory (file) ())

(defun make-file (path)
  (make-instance 'file :absolute-pathname path))

(defun make-directory (path)
  (make-instance 'dir :absolute-pathname path))
